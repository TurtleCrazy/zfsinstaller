#!/bin/sh

DESTDISKS="ada0 ada1"
DESTDIR="/mnt/zfs"
ZPOOL="zroot"

SCRIPTBASE=${PWD}

# check if the zpool already exists

ZPOOL_EXISTS=` zpool list -H | cut -f 1 | grep "${ZPOOL}"` 
if [ "${?}" -ne 1 ]; then
	echo "One zpool called ${ZPOOL} already exists, you have to destroy it first:"
	echo "	# zpool destroy ${ZPOOL}"
	exit 1
fi

# check if the disk is ok for writing



sysctl kern.geom.part.mbr.enforce_chs=0

for D in ${DESTDISKS}; do
	if [ -z "${DESTDISK_1}" ]; then
		DESTDISK_1=${D}
	fi

	echo checking sanity ${D} 
	echo =============
	gpart show ${D}  1>/dev/null
	if [ "${?}" -eq 0 ]; then

		gpart destroy -F ${D}
		if [ "${?}" -ne 0 ]; then
			echo unable to reset ${D}
			exit 1
		fi
	fi
	echo partitioning ${D} 
	echo =============
	N=$( echo ${D} | tr -c -d '0-9' )
	gpart create -s gpt ${D}
	gpart add -a 4k -t efi -s 200M -l efiboot${N} ${D} 
	gpart bootcode -p /boot/boot1.efifat -i 1 ${D} 
	gpart add -a 4k -t freebsd-boot -s 512k -l gptboot${N} ${D} 
	gpart bootcode -b /boot/pmbr -p /boot/gptzfsboot -i 2 ${D} 
	gpart add -a 1m -s 32G -t freebsd-swap -l swap${N} ${D} 
	gpart add -a 1m -t freebsd-zfs -l zfs${N} ${D} 
	gpart set -a active ${D}
	gpart show ${D}  
done

sysctl vfs.zfs.min_auto_ashift=12

echo set up zfs pool: ${ZPOOL} alt mount: ${DESTDIR}
echo =================

zpool create  -m none -f -R ${DESTDIR} ${ZPOOL} /dev/gpt/zfs*
if [ "${?}" -ne 0 ]; then
	echo "unable to create zpool"
	exit 1
fi


for I in ROOT VAR HOME DATA; do
	zfs create -o mountpoint=none -o canmount=off  ${ZPOOL}/${I}
done

zfs create -o atime=off -o mountpoint=/ ${ZPOOL}/ROOT/master

zfs create -o mountpoint=/usr/obj ${ZPOOL}/ROOT/obj
zfs create -o compression=off -o mountpoint=/usr/src ${ZPOOL}/ROOT/src
zfs create -o mountpoint=/usr/doc -o compression=on ${ZPOOL}/ROOT/doc
# useless ? as far /tmp mountpoint will be mounted by tmpfs
zfs create -o mountpoint=/tmp ${ZPOOL}/ROOT/tmp

zfs create -o mountpoint=/usr/local ${ZPOOL}/LOCAL
zfs create -o mountpoint=/usr/local/etc ${ZPOOL}/LOCAL/config


zfs create -o mountpoint=/var ${ZPOOL}/VAR/master
zfs create -o mountpoint=/var/crash ${ZPOOL}/VAR/crash
zfs create -o exec=off -o setuid=off -o mountpoint=/var/db ${ZPOOL}/VAR/db
zfs create -o compression=lz4 -o exec=on -o setuid=off -o mountpoint=/var/db/pkg ${ZPOOL}/VAR/db/pkg
zfs create -o compression=on -o exec=off -o setuid=off -o mountpoint=/var/mail ${ZPOOL}/VAR/mail
zfs create -o compression=on -o exec=off -o setuid=off -o mountpoint=/var/log ${ZPOOL}/VAR/log
zfs create -o exec=off -o setuid=off -o mountpoint=/var/run ${ZPOOL}/VAR/run
zfs create -o exec=off -o setuid=off -o mountpoint=/var/tmp ${ZPOOL}/VAR/tmp

zfs create -o mountpoint=/usr/home ${ZPOOL}/HOME/master

zpool set bootfs=${ZPOOL}/ROOT/master ${ZPOOL}

chmod 1777 ${DESTDIR}/tmp ${DESTDIR}/var/tmp

zfs list -r ${ZPOOL}

cd ${DESTDIR}/tmp
if [ "${?}" -ne 0 ]; then
	echo zfs mountpoints are not ready
	exit 1
fi

echo Fetching and Extracting base files into ${DESTDIR}
echo =======================================


for I in base.txz kernel.txz lib32.txz; do
	fetch http://ftp.freebsd.org/pub/FreeBSD/releases/amd64/12.2-RELEASE/${I}
	tar --unlink -pJxf ${I} -C ${DESTDIR}
done

echo writing configuration files
echo ==========================

cat << EOF >> ${DESTDIR}/etc/fstab
/dev/${DESTDISK_1}p3 none        swap    sw  0   0
tmpfs	/tmp	tmpfs	rw,mode=1777	0	0
EOF

cat << EOF >> ${DESTDIR}/boot/loader.conf
zfs_load=YES
tmpfs_load=YES
EOF

cat << EOF >> ${DESTDIR}/boot/loader.conf.local
vfs.root.mountfrom="zfs:${ZPOOL}/ROOT/master"
EOF

COPYCAT="/etc"
cd ${SCRIPTBASE}${COPYCAT} || exit 1
echo installing files into ${COPYCAT}
for I in *.conf*; do
        if [ -r "${I}" -a -f ${I} ]; then
                install -o root -g wheel -m 0644  ${I} ${DESTDIR}${COPYCAT}/
        fi
done
COPYCAT="/etc/rc.conf.d"
cd ${SCRIPTBASE}${COPYCAT} || exit 1
echo installing files into ${COPYCAT}
install -o root -g wheel -d -m 0755 ${DESTDIR}${COPYCAT} || exit 1
for I in *; do
        if [ -r "${I}" -a -f ${I} ]; then
			install  -o root -g wheel -m 0644 ${I} ${DESTDIR}${COPYCAT}/
		fi
done
COPYCAT="/etc/rc.conf.d/network"
cd ${SCRIPTBASE}${COPYCAT} || exit 1
install -o root -g wheel -d -m 0755 ${DESTDIR}${COPYCAT} || exit 1
echo installing files into ${COPYCAT}
install  -o root -g wheel -m 0644 * ${DESTDIR}${COPYCAT}/

chroot ${DESTDIR} passwd
chroot ${DESTDIR} adduser

read -p "Do you wish to open a shell ?" yn
case $yn in
	[Nn]* ) exit 0;;
	[Yy]* ) ;;
	* ) echo "Please answer yes or no.";;
esac
chroot ${DESTDIR} tcsh
exit 0

